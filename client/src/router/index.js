import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Accounting from '@/components/Accounting'
import Search from '@/components/Search'
import Chargeback from '@/components/Chargeback'
import routers from './routers'


Vue.use(Router)

const router = new Router({
  mode : 'history',
  routes : routers
})

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path : '/accounting',
      name : 'Accounting',
      component : Accounting
    },
    {
      path : '/search',
      name : 'Search',
      component : Search
    },
    {
      path : '/chargeback',
      name : 'Chargeback',
      component : Chargeback
    }
  ]
})

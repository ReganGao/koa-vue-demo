
let Koa = require('koa');
let Router = require('koa-router');
let bodyParser = require('koa-bodyparser');
let cors = require('koa-cors');
let async = require('async');
let mongodb = require('./mongoose');

mongodb.loadModels();
mongodb.startConnect();

let api_handle = require('./apiHandle');

const app = new Koa();
const router = new Router();

app.use(bodyParser())

router
	.post('/api' , async (ctx, next) => {
		var req_body = ctx.request.body;
		var res = await deal_api(req_body);
		ctx.body = res;
		
	})


async function deal_api(req_body){
	return new Promise(function(resolve, reject){
		api_handle.deal_api(req_body, function(err, result){
			if(err){
				reject(err);
			}else{
				console.log("result : " + JSON.stringify(result));
				resolve(result);
			}
		});
	})
}



app.use(router.routes()).use(router.allowedMethods());

app.listen(3000);

console.log("server is running : http://0.0.0.0:3000");
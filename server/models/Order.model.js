
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


var OrderSchema = new Schema({
	cellerId : {
		type : String
	},
	customerId : {
		type : String
	},
	status : {
		type : Number,   
		default : 0  
	},
	payType : {
		type : Number,  
		default : 0
	},
	type : {
		type : Number   // 0:正常  1:退单
	},
	chargebackId : {     // 如果type为1  则chargebackId为原订单order_id
		type : String
	},
	time : {
		type : Number
	}
})

mongoose.model('Order', OrderSchema);
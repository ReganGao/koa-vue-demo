
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


var OrderDetailSchema = new Schema({
	orderId : {
		type : String
	},
	cellerId : {
		type : String
	},
	customerId : {
		type : String
	},
	productId : {
		type : String
	},
	productName : {
		type : String
	},
	count : {
		type : Number
	},
	totalMoney : {
		type : Number
	},
	time : {
		type : Number
	}
})

mongoose.model('OrderDetail', OrderDetailSchema);

import Accounting from '../components/Accounting.vue'
import Search from '../components/Search.vue'
import Chargeback from '../components/Chargeback.vue'
import Index from '../components/Index'

const routers = [
  {
    path: '/',
    component: Index,
  },
  {
    path : '/accounting',
    component : Accounting
  },
  {
    path : '/search',
    component : Search
  },
  {
    path : '/chargeback',
    component : Chargeback
  }
]

export default routers
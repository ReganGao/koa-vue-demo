
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


var CellerSchema = new Schema({
	name : {
		type : String,
		default : '',
		unique : true
	}
})

mongoose.model('Celler', CellerSchema);
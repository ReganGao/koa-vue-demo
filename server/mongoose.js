let mongoose = require('mongoose');
let async = require('async')
let config = {
	uri : 'mongodb://127.0.0.1:27017/koaDemo',
	options : {
		useNewUrlParser : true
	}
}

module.exports.loadModels = function (callback) {
	require('./models/Celler.model.js');
	require('./models/Customer.model.js');
	require('./models/Order.model.js');
	require('./models/OrderDetail.model.js');
	require('./models/Product.model.js');
	if(callback){
		callback()
	}
}

module.exports.startConnect = function () {
	let db = mongoose.connect(config.uri, config.options, function(err){
		if(err){
			//console.log(err);
		}else{
			seedModel();
		}
	})
}

module.exports.disconnect = function (callback) {
  mongoose.disconnect(function (err) {
    console.info(chalk.yellow('Disconnected from MongoDB.'));
    callback(err);
  });
};

var Celler_1 = {
	name : 'Regan'
};
var Celler_2 = {
	name : "Kate"
};
var Product_1 = {
	productName : '生物科学',
	price : 200
};
var Product_2 = {
	productName : '化学制剂',
	price : 500
};

function seedModel(){
	var Celler = mongoose.model('Celler');
	
	Celler1 = new Celler(Celler_1);
	Celler2 = new Celler(Celler_2);
	
	
	saveUser(Celler1, Celler2);
}

function saveUser(Celler1, Celler2){
	var Product = mongoose.model('Product');
	async.parallel([
		function(cb){
			Celler1.save(function(err, result){
				if(err){
					cb(err, null);
				}else{
					var cellerId = result._id;
					var Product1 = new Product(Product_1);
					Product1.save(cb);
				}
			});
		},
		function(cb){
			Celler2.save(function(err, result){
				if(err){
					cb(err, null);
				}else{
					var cellerId = result._id;
					var Product2 = new Product(Product_2);
					Product2.save(cb);
				}
			});
		}
	],function(err, results){
		if(err){
			console.log("err : " + JSON.stringify(err));
		}else{
			console.log("seed model success!!!");
		}
	})
}
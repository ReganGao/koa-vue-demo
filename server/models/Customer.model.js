
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


var CustomerSchema = new Schema({
	name : {
		type : String,
		default : ''
	},
	phone : {
		type : Number,
		unique : true
	},
	address : {
		type : String
	}
})

mongoose.model('Customer', CustomerSchema);

var Order = require('./apis/orders.api.js');
var Product = require('./apis/products.api.js');
var User = require('./apis/users.api.js');


exports.deal_api = function(req_body, cb){
	var action = req_body.action;
	if(action === 'cellerList'){
		User.cellerList(function(err, result){
			cb(err, result);
		})
	}else if(action === 'productList'){
		Product.productList(function(err, result){
			cb(err, result);
		})
	}else if(action === 'createOrder'){
		Order.createOrder(req_body, function(err, result){
			cb(err, result);
		})
	}else if(action === 'orderByTime'){
		Order.orderByTime(req_body, function(err, result){
			cb(err, result);
		})
	}else if(action === 'orderById'){
		Order.orderById(req_body, function(err, result){
			cb(err, result);
		})
	}else if(action === 'account'){
		Order.account(req_body, function(err, result){
			cb(err, result);
		})
	}
}
let mongoose = require('mongoose');
let async = require('async');

// var Celler = require('../models/Celler.model.js');
// var Customer = require('../models/Customer.model.js');
var Order = mongoose.model('Order');
var OrderDetail = mongoose.model('OrderDetail');
var Product = mongoose.model('Product');
var Celler = mongoose.model('Celler');
exports.createOrder = function(reqBody, callback){
	console.log("reqBody : " + JSON.stringify(reqBody, null, '\t'));
	var date = new Date();
	var timeStap = date.getTime();
	var order = {
		cellerId : reqBody.cellerId,
		customerId : '1234567890',
		status : 0,
		payType : 0,
		type : 0,
		time : timeStap,
		chargebackId : ''
	};
	order = JSON.parse(JSON.stringify(order));
	if(reqBody.count < 0){
		order.chargebackId = reqBody.orderId;
	}
	console.log("order : " + JSON.stringify(order));
	var OrderData = new Order(order);
	
	async.waterfall([
		function(cb){
			OrderData.save(cb);
		},
		function(obj, cb){
			var orderId = obj._id;
			if(reqBody.count < 0){
				orderId = reqBody.orderId;
			}
			var orderDetail = {
				orderId : orderId,
				cellerId : obj.cellerId,
				customerId : obj.customerId,
				productId : reqBody.productId,
				count : reqBody.count,
				totalMoney : reqBody.totalMoney,
				time : timeStap,
				productName : reqBody.productName
			};

			var OrderDetailData = new OrderDetail(orderDetail);
			OrderDetailData.save(cb)
		}
	],function(err, result){
		callback(err, result);
	})
};

exports.orderByTime = function(reqBody, callback){
	console.log("reqBody : " + JSON.stringify(reqBody));
	OrderDetail.find({time : {$gt : reqBody.startTime, $lt : reqBody.endTime}}).exec(callback);
}

exports.orderById = function(reqBody, callback){
	OrderDetail.find({orderId : reqBody.orderId}).exec(function(err, result){
		if(err){
			callback(err, null);
		}else{
			var order = {
				orderId : result[0].orderId,
				productName : result[0].productName,
				count : 0,
				cellerId : result[0].cellerId
			};
			var lastCount = 0;
			var lastMoney = 0;
			async.each(result, function(orderObj, escb){
				lastCount = lastCount + orderObj.count;
				lastMoney = lastMoney + orderObj.totalMoney;
				escb(null, null)
			},function(error){
				if(error){
					callback(error, null)
				}else{
					order.count = lastCount;
					order.price = lastMoney/lastCount;
					callback(null, order);
				}
			})
		}
	});
}

exports.account = function(reqBody, callback){
	console.log("reqBody : " + JSON.stringify(reqBody, null, '\t'));
	var celler = reqBody.sellerName;
	var startTime = reqBody.start;
	var endTime = reqBody.end;
	var endDate = new Date(endTime);
	var endMonth = endDate.getMonth()+1;
	endTime = endDate.setMonth(endMonth);
	console.log("endTime : " + endTime);

	if(celler === 'All'){
		OrderDetail.find({time : {$gt : startTime, $lt : endTime}}).exec(function(err, result){
			var lastRes = {};
			if(err){
				callback(err, null);
			}else{
				var totalMoney = 0;
				if(result.length){
					async.each(result, function(orderObj, escb){
						totalMoney = orderObj.totalMoney + totalMoney;
						escb()
					},function(error){
						if(error){
							callback(err, null);
						}else{
							lastRes.performance = totalMoney;
							callback(null, lastRes);
						}
					})
				}else{
					lastRes.performance = 0;
					callback(null, lastRes);
				}
			}
		})
	}else{
		async.waterfall([
			function(cb){
				Celler.findOne({name : reqBody.sellerName}).exec(function(err, cellerObj){
					console.log("");
					if(err){
						cb(err, null);
					}else{
						cb(null, cellerObj);
					}
				})
			},
			function(cellerObj, cb){
				var cellerId = cellerObj._id;
				var lastRes = {};
				OrderDetail.find({cellerId : cellerId, time : {$gt : startTime , $lt : endTime}}).exec(function(err, orders){
					console.log("orders : " + JSON.stringify(orders));
					if(err){
						cb(err, null);
					}else{
						if(orders.length){
							var totalMoney = 0;
							async.each(orders, function(orderObj, escb){
								totalMoney = totalMoney + orderObj.totalMoney;
								escb();
							},function(err){
								lastRes.performance = totalMoney;
								callback(null, lastRes);
							})
						}else{
							lastRes.performance = 0;
							cb(null, lastRes);
						}
					}
				})
			}
		],function(err, result){
			if(err){
				callback(err, null);
			}else{
				console.log("result : " + JSON.stringify(result));
				callback(null, result)
			}
		})
	}
}



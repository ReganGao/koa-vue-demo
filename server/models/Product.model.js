
let mongoose = require('mongoose');
let Schema = mongoose.Schema;


var ProductSchema = new Schema({
	productName : {
		type : String,
		unique : true
	},
	price : {
		type : Number
	}
})

mongoose.model('Product', ProductSchema);